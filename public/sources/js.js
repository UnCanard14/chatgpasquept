let timePerLetter = 80;
let newLineCharacter = "\n";

var out = $('#output')[0];

beginSentence =  [
  "Je ne peux pas répondre pour le moment, ",
  "Je ne dispose pas assez d'information pour répondre, en revanche ",
  "Il n'est pas possible de fournir une réponse exacte, mais ",
  "J'entends bien la question, mais là franchement ",
  "Je ne comprends pas, par contre ",
  "Il doit y avoir une erreur car ",
  "Je ne peux pas prendre en compte votre question, ",
  "Désolé, ",
  "Veuillez m'excuser, ",
  "Ouais ouais c'est cool ",
  "Ne me parle pas ",
  "Attends je te laisse ",
  "Ta gueule, ta gueule, ",
  "Une erreur est survenue, ",
  "Ouais OK, "
]
endSentence = [
  "je crois que je viens de chier.",
  "j'ai fait caca.",
  "j'ai chié ?",
  "je viens de faire un gros caca.",
  "je crois que je viens de déposer un colis.",
  "je crois que j'ai bombardé la zone.",
  "je sens que c'était pas qu'un pet.",
  "je qu'il y a des petits morceaux.",
  "j'ai chié ma race",
  "ça dégouline...",
  "il y en a meme dans mes chaussures...",
  "je crois que ça coule sur mon molet...",
  "j'ai chié un truc énorme.",
  "je viens de chier une énorme merde.",
  "je doit couler un bronze.",
  "j'ai démoulé un cake.",
  "j'ai laché un gros colombin.",
  "j'ai des potes à déposer à la piscine.",
  "j'ai laché un étron.",
  "j'ai le cigare au bout des levres.",
  "j'ai la taupe qui me pousse au trou",
  "j'ai pas que pété."
]

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}

function printOut(str) {
  var i = 0;
  (function main() {
    var char = str[i++];        
    if (char === newLineCharacter) {
        out.appendChild(document.createElement("br"));
    } else {
        out.appendChild(document.createTextNode(char));
    }
    
    if(i < str.length)
      timer = setTimeout(main, Math.random() * timePerLetter);
  })();
}

$("#input").on("keyup", function(e) {
  if (event.keyCode === 13) {
   event.preventDefault();
    go();
  }
});

document.getElementById("svg").addEventListener("click", function(event) {
    go();              
});

var timer = "";

function go() {
   var in_text = $("#input").val();
   if (!$("body").is("loader")) {
       clearTimeout(timer);
       // $("body").addClass("loader");
       $("#ok").show();
       $("#input_echo").html(in_text);
       $("#input").val("");
       $("#output").html("");
       // $.ajax({
       //     "url":"gpt.php",
       //     "type":"POST",
       //     "data": {
       //         "text": in_text
       //     },
       //     "dataType": "json",
       //     "success": function(msg) {
       //        $("body").removeClass("loader");
       //        printOut(msg.body);
       //     }
       // });
       printOut(beginSentence[randomIntFromInterval(0, beginSentence.length-1)] + endSentence[randomIntFromInterval(0, endSentence.length-1)]);
   }
}


function launch_countdown() {

	setTimeout(function() {
		$(window).scrollTop(0);
	}, 1000);
	var currentDate = new Date();
	var nextHour = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), currentDate.getHours()+1, 0, 0);
	var countDownDate = nextHour.getTime();

	var x = setInterval(function() {
	  var now = new Date().getTime();

	  var distance = countDownDate - now;

	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	  document.getElementById("countdown").innerHTML = "GRÈVE SURPRISE !<br> On arrête le boulot !<br>La grève prendra fin dans "+minutes + "m " + seconds + "s ";

	  if (distance < 0) {
			document.location.href = document.location.href;
	  }
	}, 1000);
}


jQuery("#printer").click(function() {
   window.print(); 
});